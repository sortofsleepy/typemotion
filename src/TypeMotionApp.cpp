#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/ImageIo.h"
#include "cinder/gl/Texture.h"
#include "Contours.h"
#include "HydrantSystem.h"
#include "cxCv.h"
#include <vector>

using namespace ci;
using namespace ci::app;
using namespace std;
using namespace cxCv;

class TypeMotionApp : public AppNative {
  public:
	void setup();
    void update();
	void draw();
	Surface image;
	gl::Texture	mTexture;
    cxCv::ContourRef contourFinder;
    void prepareSettings(Settings * settings);
    
    std::vector<Vec2f> targets;
    
    //list of emiters
    std::vector<HydrantSystem> emiters;
};


void TypeMotionApp::prepareSettings(cinder::app::AppBasic::Settings *settings){
    settings->setFrameRate(60.0);
    settings->setWindowSize(1024,768);
}
void TypeMotionApp::setup()
{
    image = cxCv::Helpers::loadImage("test.png");
    contourFinder = Contours::create();
    targets = contourFinder->findContours(image);
    
    vector<Vec2f>::iterator it;
    for(int i = 0;i<targets.size();i+=8){
        emiters.push_back(HydrantSystem(targets.at(i)));
    }
   
    
    vector<HydrantSystem>::iterator sys;
    for(sys = emiters.begin();sys != emiters.end();++sys){
        sys->addParticle(2);
        
    }

}

void TypeMotionApp::update(){
    vector<HydrantSystem>::iterator sys;
    for(sys = emiters.begin();sys != emiters.end();++sys){
         sys->addParticle(2);
        sys->update();
    }
}

void TypeMotionApp::draw()
{
	gl::clear(Color(1,1,1));
    gl::pushMatrices();
    gl::translate(50,0);
    vector<HydrantSystem>::iterator sys;
    for(sys = emiters.begin();sys != emiters.end();++sys){
        sys->draw();
    }
	  
    gl::popMatrices();
}

CINDER_APP_NATIVE( TypeMotionApp, RendererGl )
