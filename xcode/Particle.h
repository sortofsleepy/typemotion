//
//  Particle.h
//  KineticLettering
//
//  Created by Joseph Chow on 1/13/14.
//
//

#ifndef KineticLettering_Particle_h
#define KineticLettering_Particle_h

#include "cinder/Vector.h"
#include "cinder/Color.h"
#include "DisplayObject.h"


class Particle : public DisplayObject{
    
    float life;
    ci::Vec3f target;
    std::vector<Particle> neighbors;
    ci::ColorA color;
    bool isDying;
   
public:
    ci::Vec3f normalVel;
    bool isDead;
    Particle();
    Particle(ci::Vec3f newtarget);
    void setLife(float newlife);
    void toggleLifecycle();
    void setLoc(ci::Vec3f newloc);
    void setup();
    void update();
    void draw();
    float angle;
    

};




#endif
