//
//  Emiter.cpp
//  KineticLettering
//
//  Created by Joseph Chow on 1/13/14.
//
//

#include "Emiter.h"
using namespace ci;
using namespace ci::app;
using namespace std;

Emiter::Emiter(int max){}
Emiter::Emiter(Vec3f pos,int max){
    loc = pos;
    maxparticles=0;
}
Emiter::Emiter(Vec2f pos,int max){
    loc.x = pos.x;
    loc.y = pos.y;
    loc.z = 0;
    maxparticles = 0;
}





void Emiter::addParticle(Particle p){
    if(particles.size() < maxparticles){
        particles.push_back(p);
    }
}
void Emiter::addParticle(int num){
    if(particles.size() < maxparticles){
        for(int i = 0;i<num;++i){
            particles.push_back(Particle());
        }
    }
}

void Emiter::addParticle(Vec3f newtarget){
    particles.push_back(Particle(newtarget));
}

void Emiter::update(){
    vector<Particle>::iterator it;
    for(it = particles.begin();it != particles.end();++it){
        it->update();
        
        if(it->isDead){
            particles.erase(it);
        }
    }
}

void Emiter::draw(){
    vector<Particle>::iterator it;
    for(it = particles.begin();it != particles.end();++it){
        it->draw();
    }
 
    glPointSize(1.0f);
    glBegin(GL_POINTS);
        gl::vertex(loc.x,loc.y);
    glEnd();
    
}