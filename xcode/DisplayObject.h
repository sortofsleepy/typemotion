//
//  DisplayObject.h
//  NatureOfCode
//
//  Created by Joseph Chow on 12/27/13.
//
//

#ifndef __NatureOfCode__DisplayObject__
#define __NatureOfCode__DisplayObject__

#include "cinder/Vector.h"
#include "cinder/Color.h"
#include <iostream>
#include <vector>
#include <string>


class DisplayObject {
  

public:
    //general properties about the object
    int mass;
    float radius;
    ci::Vec3f accel,vel,loc,pos;
    
    //color
    ci::Color itemColor;
    
    //friction coefficient
    float f_c;
    
    /*====== FRICTION ============ */
    //normal force
    float normal_force;
    ci::Vec3f friction;
    
    float toRadians(int deg);

    
     /*====== FUNCTIONS ============ */
    DisplayObject();
    
    void addForce(ci::Vec3f force);
    virtual void updatePhysics();
    void checkEdges(std::string edgeReaction="bounce");
    
    void resetFrame();
    
    //functions to deal with what happens when
    //object reaches window boundries.
    void bounceEdges();
    void reAppear();
    
    //adds friction to the object
    void addFriction(float coefficent=0.01,float normal=1);
};
#endif /* defined(__NatureOfCode__DisplayObject__) */
