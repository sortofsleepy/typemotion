//
//  Particle.cpp
//  KineticLettering
//
//  Created by Joseph Chow on 1/13/14.
//
//

#include "Particle.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace std;


Particle::Particle(){
    setup();
}
Particle::Particle(Vec3f newtarget){
    target = newtarget;
    setup();
}

void Particle::setup(){
    life = 1.0f;
    isDead = false;
    normalVel = Vec3f::yAxis();
    isDying = false;
   // mass = radius * radius * 0.005f;
    color = ColorA(200.0,200.0,0.0,life);
    
    //we generally want particles to DIE.
    //re-call the function to turn off.
    toggleLifecycle();
    angle = 0;
}

void Particle::toggleLifecycle(){
    if(isDying){
        isDying = false;
    }else if(!isDying){
        isDying = true;
    }
}
void Particle::update(){
    if(isDying){
        life -= 0.10000930279094839;
    }
    
    angle += 0.4;
    
    
    if(life <= 0){
        isDead = true;
    }
    updatePhysics();
    
}
void Particle::setLife(float newlife){
    life = newlife;
}
void Particle::setLoc(ci::Vec3f newloc){
    loc = newloc;
}


void Particle::draw(){
    gl::enableAlphaBlending();
    gl::color(200,0,0,life);
    glPointSize(radius);
    glBegin(GL_POINTS);
    
    gl::rotate(Vec3f(sin(angle)*0.4,cos(angle)*0.4,0));
    gl::vertex(loc.x,loc.y);
   
    glEnd();
    gl::disableAlphaBlending();
    //app::console()<<"We working";
 
}

