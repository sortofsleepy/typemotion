//
//  Emiter.h
//  KineticLettering
//
//  Created by Joseph Chow on 1/13/14.
//
//

#ifndef KineticLettering_Emiter_h
#define KineticLettering_Emiter_h
#include "Particle.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Fbo.h"
#include <vector>
#include <string>
#include "cinder/Rand.h"


class Emiter{
   


public:
    Emiter(int max=100);
    Emiter(ci::Vec3f pos,int max=100);
    Emiter(ci::Vec2f pos,int max=100);
    std::vector<Particle> particles;
    

    
    //where the Emiter sits
    ci::Vec3f loc;
    
    //max number of particles
    int maxparticles;

    //adding particles
    virtual void addParticle(int num=1);
    virtual void addParticle(ci::Vec3f newtarget);
    virtual void addParticle(Particle p);
    virtual void update();
    virtual void draw();

};

#endif

