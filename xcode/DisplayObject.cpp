//
//  DisplayObject.cpp
//  NatureOfCode
//
//  Created by Joseph Chow on 12/27/13.
//
//

#include "DisplayObject.h"
#include "cinder/app/AppNative.h"
#include "cinder/Rand.h"


using namespace ci;
using namespace std;

#define PI 3.14149

DisplayObject::DisplayObject(){
    mass = 4;
    radius = 4;
    
}

void DisplayObject::addForce(ci::Vec3f force){
    Vec3f copy = force;
    copy /= mass;
    accel += copy;
   
    
}

void DisplayObject::updatePhysics(){
    vel += accel;
    loc += vel;
    //checkEdges();
    
  
}

void DisplayObject::resetFrame(){
    accel *= 0;
}

void DisplayObject::checkEdges(string edgeReaction){
    if(edgeReaction == "bounce"){
        bounceEdges();
    }else if(edgeReaction == "appear"){
        reAppear();
    }
}


void DisplayObject::reAppear(){
    if (loc.x < -radius){
     loc.x = app::getWindowWidth()+radius;
    }
    
    if(loc.y < -radius){
        loc.y = app::getWindowHeight()+radius;

    }
    
 
    if (loc.x > app::getWindowWidth()+radius) loc.x = -radius;
    if (loc.y > app::getWindowHeight()+radius) loc.y = -radius;
}
void DisplayObject::bounceEdges(){
    if(loc.x > app::getWindowWidth()){
        loc.x = app::getWindowWidth();
        vel.x *= -1;
        
    
    }else if(loc.x < 0){
        loc.x = 0;
        vel.x *= -1;
     
    }
    
    
    if(loc.y  > app::getWindowHeight()){
        loc.y = app::getWindowHeight();
        vel.y *= -1;
    }else if(loc.y < 0){
        loc.y = 0;
        vel.y *= -1;
    }
}


void DisplayObject::addFriction(float coefficent, float normal){
    float mag = coefficent * normal;
    friction = vel;
    friction *= -1;
    friction.normalize();
    friction *= mag;
    addForce(friction);
}

float DisplayObject::toRadians(int deg){
    //return toRadians(deg);
    return 2 * PI * (deg / 360);
}