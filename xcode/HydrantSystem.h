//
//  HydrantSystem.h
//  KineticLettering
//
//  Created by Joseph Chow on 1/21/14.
//
//

#ifndef __KineticLettering__HydrantSystem__
#define __KineticLettering__HydrantSystem__

#include <iostream>
#include "Emiter.h"
#include <vector>
#include "cinder/Rand.h"

class HydrantSystem : public Emiter{

public:
    HydrantSystem();
    HydrantSystem(ci::Vec2f pos);
    void addParticle();
    void addParticle(int num);
    
    void update();
    friend Emiter;
};

#endif /* defined(__KineticLettering__HydrantSystem__) */
