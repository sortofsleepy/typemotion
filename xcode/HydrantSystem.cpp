//
//  HydrantSystem.cpp
//  KineticLettering
//
//  Created by Joseph Chow on 1/21/14.
//
//

#include "HydrantSystem.h"
using namespace ci;
using namespace ci::app;
using namespace std;
HydrantSystem::HydrantSystem(){}
HydrantSystem::HydrantSystem(ci::Vec2f pos){

    loc.x = pos.x;
    loc.y = pos.y;
    loc.z = 0;

}
void HydrantSystem::addParticle(){
    Particle p;
    p.setLoc(loc);
    p.accel = Vec3f(0,0.05,0);
    p.vel = Vec3f(randFloat(-1,1),randFloat(-2,2),0);
    p.addForce(Rand::randVec3f());
    particles.push_back(p);
}

void HydrantSystem::addParticle(int num){
    for(int i = 0;i<num;++i){
        Particle p;
        p.setLoc(loc);
        p.accel = Vec3f(0,0.05,0);
        p.vel = Vec3f(randFloat(-1,1),randFloat(-2,2),0);
        
        p.addForce(Rand::randVec3f());
        p.addForce(Rand::randVec3f());
        particles.push_back(p);
    }
}


void HydrantSystem::update(){
    vector<Particle>::iterator it;
    for(it = particles.begin();it != particles.end();++it){
        it->update();
        it->resetFrame();
        
        if(it->isDead){
            particles.erase(it);
        }
    }
}